//
//  SplashScreen.h
//  MNST-mobile
//
//  Created by Marcus Rupley on 10/29/17.
//

#ifndef SplashScreen_h
#define SplashScreen_h

#include <stdio.h>


#include "cocos2d.h"

class SplashScreen : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    void onEnter();
    
    void finishSplash(float dt);
    
    // implement the "static create()" method manually
    CREATE_FUNC(SplashScreen);
};


#endif /* SplashScreen_h */
