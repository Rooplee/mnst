#ifndef __MAINMENU_SCENE_H__
#define __MAINMENU_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;

class MainMenuScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MainMenuScene);
private:
    PageView* pageView;
    void pageviewHelperCallback(cocos2d::Ref* pSender, int page);
    void playButtonCallback(cocos2d::Ref* pSender);
    enum pages {
        PAGE_ONE,
        PAGE_TWO,
        PAGE_THREE,
        PAGE_FOUR
    };
};

#endif // __MAINMENU_SCENE_H__
