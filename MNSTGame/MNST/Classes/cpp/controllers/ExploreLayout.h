//
//  ExploreLayout.h
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/12/17.
//

#ifndef ExploreLayout_h
#define ExploreLayout_h

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class ExploreLayout : public cocos2d::ui::Layout
{
public:
    virtual bool init();
    
    static cocos2d::ui::Layout* getLayout();
    
    // a selector callback
    void playButtonCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(ExploreLayout);
};

#endif /* ExploreLayout */
