//
//  SocialLayout.h
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/12/17.
//

#ifndef SocialLayout_h
#define SocialLayout_h

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class SocialLayout : public cocos2d::ui::Layout
{
public:
    virtual bool init();
    
    static cocos2d::ui::Layout* getLayout();
    
    // a selector callback
    void playButtonCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(SocialLayout);
};

#endif /* SocialLayout_h */
