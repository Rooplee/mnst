#include "MainMenuScene.h"
#include "SimpleAudioEngine.h"
#include "PlayGameScene.h"
#include "StoreLayout.h"
#include "PlayLayout.h"
#include "ExploreLayout.h"
#include "SocialLayout.h"
#include "Monster.h"

#define TOTAL_PAGES_NUM 4

USING_NS_CC;

Scene* MainMenuScene::createScene()
{
    return MainMenuScene::create();
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{

    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    Texture2D *bgTexture = Director::getInstance()->getTextureCache()->addImage("stoneBackgroundLarge.png");
    const Texture2D::TexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
    Sprite *background = Sprite::createWithTexture(bgTexture, Rect(0, 0, visibleSize.width, visibleSize.height));
    background->getTexture()->setTexParameters(&tp);
    background->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
    this->addChild(background, 0);
    
    pageView = PageView::create();
    pageView->setContentSize(visibleSize);
    pageView->addPage(StoreLayout::create());
    pageView->addPage(PlayLayout::create());
    pageView->addPage(ExploreLayout::create());
    pageView->addPage(SocialLayout::create());
    pageView->setCurrentPageIndex(1);
    this->addChild(pageView, 1);

    Monster* monst = new Monster();
    
    float spritex = origin.x;
    float spritey = origin.y + 20;
    for(int i = 0; i < TOTAL_PAGES_NUM; i++)
    {
        auto button = Button::create();
        button->loadTextureNormal("HelloWorld.png");
        button->setPosition(Vec2(spritex + button->getContentSize().width * 0.75 + visibleSize.width * i * 0.25,
                         spritey + button->getContentSize().height/2));
        button->addTouchEventListener(
                                      [=](Ref* /*sender*/, cocos2d::ui::Widget::TouchEventType type) {
                                          switch (type) {
                                              case cocos2d::ui::Widget::TouchEventType::ENDED: {
                                                  MainMenuScene::pageviewHelperCallback(this, i);
                                                  break;
                                              }
                                              default: {
                                                  break;
                                              }
                                          }
                                      });
        this->addChild(button, 1);
    }
    
    return true;
}


void MainMenuScene::playButtonCallback(Ref* pSender)
{
    Director::getInstance()->replaceScene(PlayGameScene::createScene());
}

void MainMenuScene::pageviewHelperCallback(Ref* pSender, int page)
{
    pageView->scrollToPage(page);
}

