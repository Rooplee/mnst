//
//  StoreLayout.cpp
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/12/17.
//

#include "StoreLayout.h"
#include "PlayGameScene.h"

USING_NS_CC;
using namespace cocos2d::ui;

bool StoreLayout::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    this->setContentSize(visibleSize);

    auto label = Label::createWithTTF("STORE", "fonts/Marker Felt.ttf", 120);
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));
    this->addChild(label, 1);
    
    return true;
}

void StoreLayout::playButtonCallback(Ref* pSender)
{
    Director::getInstance()->replaceScene(PlayGameScene::createScene());
}
