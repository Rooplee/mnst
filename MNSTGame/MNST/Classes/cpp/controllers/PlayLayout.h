//
//  PlayLayout.h
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/12/17.
//

#ifndef PlayLayout_h
#define PlayLayout_h

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class PlayLayout : public cocos2d::ui::Layout
{
public:
    virtual bool init();
    
    static cocos2d::ui::Layout* getLayout();
    
    void playButtonCallback(cocos2d::Ref* pSender);
    
    CREATE_FUNC(PlayLayout);
};

#endif /* PlayLayout_h */
