//
//  PlayLayout.cpp
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/12/17.
//

#include "PlayLayout.h"
#include "PlayGameScene.h"

USING_NS_CC;
using namespace cocos2d::ui;

// on "init" you need to initialize your instance
bool PlayLayout::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    this->setContentSize(visibleSize);
    
    auto label = Label::createWithTTF("Play", "fonts/Marker Felt.ttf", 120);
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));
    this->addChild(label, 1);
    
    cocos2d::ui::Button *button = cocos2d::ui::Button::create();
    button->setScale9Enabled(true);
    button->loadTextureNormal("buttons/buttonGrey.png");
    button->setTitleText("Play");
    button->setTitleFontSize(72);
    auto lbl_size = button->getTitleRenderer()->getContentSize();
    
    button->setContentSize(
                           Size(
                                lbl_size.width + 50,
                                lbl_size.height + 30
                                )
                           );
    
    float x = origin.x + visibleSize.width/2;
    float y = origin.y + button->getContentSize().height/2 + 200;
    button->setPosition(Vec2(x,y));
    button->addTouchEventListener(
                                  [this](Ref* /*sender*/, cocos2d::ui::Widget::TouchEventType type) {
                                      switch (type) {
                                          case cocos2d::ui::Widget::TouchEventType::ENDED: {
                                              PlayLayout::playButtonCallback(this);
                                              break;
                                          }
                                          default: {
                                              break;
                                          }
                                      }
                                  });
    addChild(button, 1);
    
    return true;
}

void PlayLayout::playButtonCallback(Ref* pSender)
{
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, PlayGameScene::createScene(), Color3B(0,0,0)));
}
