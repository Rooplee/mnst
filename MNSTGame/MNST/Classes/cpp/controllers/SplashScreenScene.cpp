//
//  SplashScreen.cpp
//  MNST-mobile
//
//  Created by Marcus Rupley on 10/29/17.
//

#include "SplashScreenScene.h"
#include "SimpleAudioEngine.h"
#include "MainMenuScene.h"

USING_NS_CC;
using namespace cocos2d;

cocos2d::Scene* SplashScreen::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SplashScreen::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

void SplashScreen::onEnter(){
    Layer::onEnter();
    this->scheduleOnce(schedule_selector(SplashScreen::finishSplash),2.0f); // in seconds
}

void SplashScreen::finishSplash(float dt){
    Director::getInstance()->replaceScene(MainMenuScene::createScene());
}

// on "init" you need to initialize your instance
bool SplashScreen::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    auto sprite = Sprite::create("SplashScreen.png");
    
    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 , visibleSize.height/2 ));
    
    // add the sprite as a child to this layer
    this->addChild(sprite, 0);
    
    return true;
}
