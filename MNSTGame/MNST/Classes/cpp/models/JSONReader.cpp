#include "JSONReader.h"

JsonReader* JsonReader::m_pJsonReader = NULL;

JsonReader::JsonReader() {
    m_pBytes = NULL;
    m_nRowId = -1;
}

JsonReader::~JsonReader() {
    purge();
}

JsonReader* JsonReader::getInstance() {
    if (m_pJsonReader == NULL) {
        m_pJsonReader = new JsonReader();
    }
    return m_pJsonReader;
}

void JsonReader::read(const char *fileName) {
    ssize_t size = 0;
    m_sFileName = fileName;
    string jsonFile = m_sFileName + ".json";
    string jsonpath = CCFileUtils::sharedFileUtils()->fullPathForFilename(jsonFile.c_str());
    m_pBytes = CCFileUtils::sharedFileUtils()->getFileData(jsonpath.c_str(), "r", &size);
    CCAssert( m_pBytes!= NULL, "JSON file not found");
    CCAssert( strcmp((char*)m_pBytes, "") != 0, "JSON file is empty");
    string load_str((const char*)m_pBytes, size);
    doc.Parse<0>(load_str.c_str());    
    if(doc.HasParseError()) {
        CCLog("Parsing for JSON file failed : %s", jsonFile.c_str());
    }
    //CCAssert(!doc.HasParseError(), doc.GetParseError());
}

void JsonReader::purge(void) {
    if(m_pBytes) {
        delete m_pBytes;
        m_pBytes = NULL;        
        m_nRowId = -1;
    }
}

void JsonReader::destroyInstance(void) {
    //DictionaryHelper::shareHelper()->purgeDictionaryHelper();
    if(m_pJsonReader) {
        purge();
        delete m_pJsonReader;
        m_pJsonReader = NULL;
    }
}

void JsonReader::moveNext(void) {    
    m_nRowId++;
}

const rapidjson::Value& JsonReader::getSubDict(void) {
    return DICTOOL->getSubDictionary_json(doc, m_sFileName.c_str(), m_nRowId);
}

int JsonReader::getRowCount(void) {
    return DICTOOL->getArrayCount_json(doc, m_sFileName.c_str());
}

int JsonReader::getInt(const char* key, int def /* = 0 */) {
    return DICTOOL->getIntValue_json(getSubDict(), key, def);
}

float JsonReader::getFloat(const char* key, float def /* = 0.0f */) {
    return DICTOOL->getFloatValue_json(getSubDict(), key, def);
}

string JsonReader::getString(const char* key, const char* def /* = "" */) {
    return DICTOOL->getStringValue_json(getSubDict(), key, def );
}

bool JsonReader::getBool(const char* key, bool def /* = false */) {
    return DICTOOL->getBooleanValue_json(getSubDict(), key, def);
}
