//
//  Monster.hpp
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/11/17.
//

#ifndef Monster_h
#define Monster_h

#include <stdio.h>
#include "cocos2d.h"
#include <iostream>
#include "JSONReader.h"

class Monster {
public:
    Monster();
    virtual ~Monster();
    virtual bool init();
    void parseJsonFile(void);

};

#endif /* Monster_h */
