//
//  Monster.cpp
//  MNST-mobile
//
//  Created by Marcus Rupley on 11/11/17.
//

#include "Monster.h"

Monster::Monster(){
    init();
}

Monster::~Monster(){
}

bool Monster::init(){
    CCLOG("INIT");
    parseJsonFile();
    return true;
}

struct Move {
    int level;
    std::string name;
};

void Monster::parseJsonFile(void) {
    struct Monster {
        int id;
        std::string name;
        int attack;
        int health;
        int defense;
        int speed;
        std::string frontImage;
        std::string backImage;
        int numberMoves;
        Move* moves;

        void Print() {
            CCLOG("ID: %d", id);
            CCLOG("NAME: %s", name.c_str());
            CCLOG("ATTACK: %d", attack);
            CCLOG("HEALTH: %d", health);
            CCLOG("DEFENSE: %d", defense);
            CCLOG("SPEED: %d", speed);
            CCLOG("FRONT: %s", frontImage.c_str());
            CCLOG("BACK: %s", backImage.c_str());
            for(int i = 0; i < numberMoves; i++){
                CCLOG("MOVE: %s, LEVEL: %d", moves[i].name.c_str(), moves[i].level);
            }
        }
    };

    JsonReader* pReader = JsonReader::getInstance();
    pReader->read("Monsters");

    int rows = pReader->getRowCount();
    CCLOG("ROWS: %d", rows);
    for (int i = 0; i < rows; i++) {
        pReader->moveNext();

        Monster* monster = new Monster();
        monster->id = pReader->getInt("Id");
        monster->name = pReader->getString("Name");
        monster->attack = pReader->getInt("Attack");
        monster->defense = pReader->getInt("Defense");
        monster->speed = pReader->getInt("Speed");
        monster->frontImage = pReader->getString("FrontImage");
        monster->backImage = pReader->getString("BackImage");


        monster->Print();
    }
    pReader->purge();
}


// void Monster::changeState() { //CharacterStates newState){
//    this->setcharacterState(newState);
//    cocos2d::CCAction *action = NULL;
//    if (newState == kStateBouncing) {
//        CCJumpTo *jumpTo = CCJumpTo::create(1.5f, this->getInitialPosition(), this->getPositionY() + getScreenSize().height/2, 1.0);
//        CCSequence *sequence = CCSequence::create(jumpTo, CCCallFuncN::create(this, callfuncN_selector(Ball::bounceMe)) ,NULL);
//        action = sequence;
//    }
//    else if (newState == kStateTapped){
//        CCMoveTo *moveTo = CCMoveTo::create(1.5f, ccp(getScreenSize().width, getScreenSize().height));
//        CCScaleTo *scaleTo = CCScaleTo::create(1.5f, 0.5f);
//        CCSpawn *spawn = CCSpawn::create(moveTo, scaleTo, NULL);
//        CCSequence *sequence = CCSequence::create(spawn, CCCallFuncN::create(this, callfuncN_selector(Ball::removeAndClean)) ,NULL);
//        action = sequence;
//    }
//    else if(newState == kStateDead){
//        this->setIsActive(false);
//        this->setVisible(false);
//        this->removeFromParentAndCleanup(true);
//    }
//    if (action != NULL) {
//        this->runAction(action);
//    }

// void Monster::updateWithDeltaTime(float deltaTime, CCArray *listOfGameObjects){
    
// }
