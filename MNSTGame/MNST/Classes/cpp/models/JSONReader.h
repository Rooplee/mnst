#ifndef __JSON_READER__
#define __JSON_READER__

#include "cocos2d.h"
#include "cocos-ext.h"

#include <string>
#include <cocostudio/DictionaryHelper.h>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;
using namespace rapidjson;
using namespace cocostudio;

class JsonReader
{
public:
    static JsonReader* getInstance();
    void read(const char *fileName);
    void purge(void);
    void destroyInstance(void);    
    void moveNext(void);
    
    int getRowCount(void);
    int getInt(const char* key, int def = 0);
    float getFloat(const char* key, float def = 0.0f);
    string getString(const char* key, const char* def = "");
    bool getBool(const char* key, bool def = false);
    
private:
    JsonReader();
    ~JsonReader();
    
    const rapidjson::Value& getSubDict(void);
    static JsonReader* m_pJsonReader;
    Document doc;
    
    unsigned char* m_pBytes;
    string m_sFileName;
    int m_nRowId;
};
#endif
