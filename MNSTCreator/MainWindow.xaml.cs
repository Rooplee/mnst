﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WpfApp1
{
    class Move
    {
        public int level { get; set; }
        public string name { get; set; }
    }
    class Effect
    {
        public string target { get; set; }
        public string element { get; set; }
        public int pow { get; set; }
        public int repeat { get; set; }
        public int delay { get; set; }
        public bool status { get; set; }
    }

    class Monster
    {
        public string name { get; set; }
        public int id { get; set; }
        public int attack { get; set; }
        public int defense { get; set; }
        public int speed { get; set; }
        public int health { get; set; }
        public List<Move> moves { get; set; }

        public string frontImage { get; set; }
        public string backImage { get; set; }

    }
    class AttackMove
    {
        public string name { get; set; }
        public string icon { get; set; }
        public int movePriority { get; set; }
        public List<Effect> effects { get; set; }
    }

    //Dynamic Visual Components
    class EffectBox
    {
        public ComboBox targets = new ComboBox();
        public ComboBox elements = new ComboBox();
        public TextBox pow = new TextBox();
        public TextBox repeat = new TextBox();
        public TextBox delay = new TextBox();
        public CheckBox status = new CheckBox();
        public Button remove = new Button();
    }
    class MonsterMoveBox
    {
        public ComboBox moves = new ComboBox();
        public TextBox level = new TextBox();
        public Button remove = new Button();
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // For list boxes, -1 = nothing selected
        const int NO_SELECTION = -1;

        // Overall variables
        static string filePrefix = "C:/Users/Roop/Documents/";
        static string previousTab = "";

        // Monster Page
        // Regular = Attack, Air = switchMNST, Fire = burn, Water = Asphyxiate, Ground = Defense
        // Electric = Speed, Steel = Ability (Copy #), Lunar = 1=Prevent 2=Reflect 3=Repeat, Nature = Health(percent)
        static String[] elements = { "Regular", "Air", "Fire", "Water", "Ground",
            "Electric", "Steel", "Nature", "Lunar"};
        static String[] statusEffects = { "", "Elemental" };
        static String[] targets = { "Opponent", "Self", "Area" };
        List<Monster> loadedMonsters;
        static int previousMonsterSelectedIndex = -1;

        //Moves Page
        List<AttackMove> loadedAttackMoves = new List<AttackMove>();
        List<MonsterMoveBox> MonsterMoves = new List<MonsterMoveBox>();
        List<EffectBox> MoveEffects = new List<EffectBox>();
        static int previousAttackMoveSelectedIndex = -1;

        public MainWindow()
        {
            InitializeComponent();
            setInitialData();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as TabControl).SelectedItem == null)
            {
                return;
            }
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;

            if (!tabItem.Equals(previousTab))
            {
                previousTab = tabItem;
                switch (tabItem)
                {
                    case "Monsters":
                        loadMonsterOntoScreen(listMNST.SelectedIndex);
                        break;
                    case "Moves":
                        loadAttackMoveOntoScreen(listMoves.SelectedIndex);
                        break;
                    case "Zones":
                        break;
                    default:
                        return;
                }
            }
        }

        private void DeserializeMonstersFromJSON(string json)
        {
            if (json.Equals(""))
            {
                return;
            }
            JObject myJsonNetObject = JObject.Parse(json);
            foreach (JObject monster in myJsonNetObject["Monsters"])
            {
                loadedMonsters.Add(CreateMonster(monster));
            }
        }

        public ComboBox CreateComboBox(String[] items)
        {
            ComboBox box = new ComboBox();
            for (int i = 0; i < items.Length; i++)
            {
                box.Items.Add(items[i]);
            }
            box.SelectedIndex = 0;

            return box;
        }

        private string SerializeMonstersToJSON()
        {
            JObject monsterObject = new JObject();
            JObject serialzedJson;
            JObject moves;
            JArray monsterArray = new JArray();
            JProperty monsterProperties;
            foreach (Monster monster in loadedMonsters)
            {
                serialzedJson = new JObject();

                serialzedJson.Add("Name", monster.name);
                serialzedJson.Add("Attack", monster.attack);
                serialzedJson.Add("Defense", monster.defense);
                serialzedJson.Add("Health", monster.health);
                serialzedJson.Add("Speed", monster.speed);

                serialzedJson.Add("Id", monster.id);
                serialzedJson.Add("FrontImage", monster.frontImage);
                serialzedJson.Add("BackImage", monster.backImage);

                moves = new JObject();
                foreach (Move move in monster.moves)
                {
                    moves.Add(move.name, move.level);
                }

                serialzedJson.Add("Moves", moves);

                monsterArray.Add(serialzedJson);
            }
            monsterProperties = new JProperty("Monsters", monsterArray);
            monsterObject.Add(monsterProperties);
            return monsterObject.ToString();
        }

        private Monster CreateMonster(JObject monsterObj)
        {
            List<Move> moves = new List<Move>();
            Monster monster = new Monster();
            monster.name = monsterObj["Name"].ToString();
            monster.attack = int.Parse(monsterObj["Attack"].ToString());
            monster.defense = int.Parse(monsterObj["Defense"].ToString());
            monster.health = int.Parse(monsterObj["Health"].ToString());
            monster.speed = int.Parse(monsterObj["Speed"].ToString());

            monster.id = int.Parse(monsterObj["Id"].ToString());
            monster.frontImage = monsterObj["FrontImage"].ToString();
            monster.backImage = monsterObj["BackImage"].ToString();

            Move newMove;

            foreach (JProperty move in (JToken)monsterObj["Moves"])
            {
                newMove = new Move();
                newMove.level = (int)move.Value;
                newMove.name = move.Name.ToString();
                moves.Add(newMove);
            }

            monster.moves = moves;

            return monster;
        }

        private Monster CreateMonster(string name)
        {
            List<Move> moves = new List<Move>();
            Monster monster = new Monster();
            monster.name = name;
            monster.attack = 0;
            monster.defense = 0;
            monster.health = 0;
            monster.speed = 0;

            monster.id = 0;
            monster.frontImage = "";
            monster.backImage = "";

            monster.moves = moves;

            return monster;
        }

        private void InitializeData()
        {
            foreach (Monster monster in loadedMonsters)
            {
                AddMonster(monster.name);
            }
        }

        //MNST CREATE BUTTON
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewMnst.Text.Length > 0)
            {
                AddMonster(txtNewMnst.Text);
                loadedMonsters.Add(CreateMonster(txtNewMnst.Text));
                txtNewMnst.Clear();
            }
        }

        private void AddMonster(string name)
        {
            if (!listMNST.Items.Contains(name))
            {
                listMNST.Items.Add(name);
                listMNST.UpdateLayout();
            }
        }

        private void btnRemoveMNST_Click(object sender, RoutedEventArgs e)
        {
            if (listMNST.SelectedIndex >= 0)
            {
                MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to remove " + listMNST.Items.GetItemAt(listMNST.SelectedIndex).ToString() + "?",
              "Remove MNST?",
              MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    loadedMonsters.RemoveAt(listMNST.SelectedIndex);
                    // Removing an item from a list immediately calls selection change
                    // setting the index to -1 prevents data from being overridden
                    previousMonsterSelectedIndex = -1;
                    listMNST.Items.RemoveAt(listMNST.SelectedIndex);
                    ResetView();
                }
            }
        }

        private void listMNST_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SaveMonster(previousMonsterSelectedIndex);
            loadMonsterOntoScreen(listMNST.SelectedIndex);
            previousMonsterSelectedIndex = listMNST.SelectedIndex;
        }

        private void SaveMonster(int monsterIndex)
        {
            if (monsterIndex < 0)
            {
                return;
            }
            Monster monster = CreateMonster(loadedMonsters[monsterIndex].name);
            monster.attack = int.Parse(txtAttack.Text);
            monster.defense = int.Parse(txtDefense.Text);
            monster.speed = int.Parse(txtSpeed.Text);
            monster.health = int.Parse(txtHealth.Text);
            if (frontImage.Source != null)
            {
                string frontImageString = frontImage.Source.ToString();
                monster.frontImage = frontImageString.Substring(frontImageString.LastIndexOf('/') + 1);
            }
            if (backImage.Source != null)
            {
                string backImageString = backImage.Source.ToString();
                monster.backImage = backImageString.Substring(backImageString.LastIndexOf('/') + 1);
            }

            List<Move> moves = new List<Move>();
            Move move = new Move();
            movePanel.Children.Clear();
            int i = 0;
            foreach (MonsterMoveBox box in MonsterMoves)
            {
                if (box.moves.SelectedItem != null)
                {
                    move.name = box.moves.SelectedItem.ToString();
                }
                else
                {
                    move.name = box.moves.Tag.ToString();
                }
                move.level = ConvertTextBoxToInt(box.level);
                moves.Add(move);
                move = new Move();
                i++;
            }
            monster.moves = moves;
            loadedMonsters.RemoveAt(monsterIndex);
            loadedMonsters.Insert(monsterIndex, monster);
        }

        private void loadMonsterOntoScreen(int monsterIndex)
        {
            if (monsterIndex == NO_SELECTION)
            {
                return;
            }
            Monster monster = loadedMonsters[monsterIndex];
            txtAttack.Text = monster.attack.ToString();
            txtDefense.Text = monster.defense.ToString();
            txtSpeed.Text = monster.speed.ToString();
            txtHealth.Text = monster.health.ToString();
            string frontImageSource = filePrefix + monster.frontImage.ToString();
            if (File.Exists(frontImageSource))
            {
                frontImage.Source = new BitmapImage(new Uri(frontImageSource));
            }
            else
            {
                frontImage.Source = null;
            }
            string backImageSource = filePrefix + monster.backImage.ToString();
            if (File.Exists(backImageSource))
            {
                backImage.Source = new BitmapImage(new Uri(backImageSource));
            }
            else
            {
                backImage.Source = null;
            }
            MonsterMoves.Clear();
            movePanel.Children.Clear();
            foreach (Move move in monster.moves)
            {
                AddMoveIndex(move);
            }
        }

        private void btnAddFront_Click(object sender, RoutedEventArgs e)
        {
            frontImage.Source = getImageFromFileBrowse();
        }

        private void btnAddBack_Click(object sender, RoutedEventArgs e)
        {
            backImage.Source = getImageFromFileBrowse();
        }

        private BitmapImage getImageFromFileBrowse()
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "png|*.png";
            Nullable<bool> result = fd.ShowDialog();
            if (result == true)
            {
                BitmapImage img = new BitmapImage();
                return new BitmapImage(new Uri(fd.FileName));
            }
            return null;
        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            if (listMNST.Items.Count > 0)
            {
                MessageBoxResult dialogResult = MessageBox.Show("Do you want to save changes to your current work?",
              "Save Changes?",
              MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    ExportChanges();
                }
            }
            listMNST.Items.Clear();
            listMNST.UpdateLayout();
            ResetView();
            setInitialData();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            SaveMonster(previousMonsterSelectedIndex);
            ExportChanges();
        }

        private bool ExportChanges()
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "json|*.json";
            Nullable<bool> result = fd.ShowDialog();
            if (result == true)
            {
                File.WriteAllText(fd.FileName, SerializeMonstersToJSON());
            }
            return result == null ? false : true;
        }
        private string ImportFile()
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "json|*.json";
            Nullable<bool> result = fd.ShowDialog();
            if (result == true)
            {
                return File.ReadAllText(fd.FileName);
            }
            return "";
        }

        private void setInitialData()
        {
            loadedMonsters = new List<Monster>();
            DeserializeMonstersFromJSON(ImportFile());
            InitializeData();
        }

        private void btnAddMonsterMove_Click(object sender, RoutedEventArgs e)
        {
            AddMoveIndex(null);
        }

        private ComboBox CreateMonsterMoveComboBox()
        {
            ComboBox box = new ComboBox
            {
                Name = "box" + MonsterMoves.Count
            };

            foreach (AttackMove attackMove in loadedAttackMoves)
            {
                box.Items.Add(attackMove.name);
            }

            box.Width = 150;
            box.HorizontalAlignment = HorizontalAlignment.Left;

            return box;
        }

        private void btnRemoveMonsterMove_Click(object sender, RoutedEventArgs e)
        {
            foreach (MonsterMoveBox box in MonsterMoves)
            {
                if (box.remove == e.Source)
                {
                    MonsterMoves.Remove(box);
                    movePanel.Children.Remove(box.moves);
                    movePanel.Children.Remove(box.level);
                    movePanel.Children.Remove(box.remove);
                    break;
                }
            }
        }

        private void AddMoveIndex(Move move)
        {
            Grid grid = new Grid();
            MonsterMoveBox monsterBox = new MonsterMoveBox();
            monsterBox.moves = CreateMonsterMoveComboBox();

            if (move == null)
            {
                move = new Move
                {
                    name = "move" + MonsterMoves.Count,
                    level = MonsterMoves.Count
                };

            }
            monsterBox.moves.Tag = move.name;
            monsterBox.moves.Text = move.name;
            monsterBox.level.Text = move.level.ToString();
            monsterBox.level.Width = 30;
            monsterBox.level.HorizontalAlignment = HorizontalAlignment.Left;
            monsterBox.remove.Width = 50;
            monsterBox.remove.HorizontalAlignment = HorizontalAlignment.Left;
            monsterBox.remove.Content = "Remove";
            monsterBox.remove.Click += btnRemoveMonsterMove_Click;

            movePanel.Children.Add(monsterBox.moves);
            movePanel.Children.Add(monsterBox.level);
            movePanel.Children.Add(monsterBox.remove);

            MonsterMoves.Add(monsterBox);
            SetComboBoxIndex(monsterBox.moves, move.name.ToString());
        }

        private void SetComboBoxIndex(ComboBox box, string name)
        {
            if (box.Items.Contains(name))
            {
                box.SelectedIndex = box.Items.IndexOf(name);
            }
        }

        private void ResetView()
        {
            txtAttack.Clear();
            txtDefense.Clear();
            txtSpeed.Clear();
            txtHealth.Clear();
            frontImage.Source = null;
            backImage.Source = null;
            MonsterMoves.Clear();
            movePanel.Children.Clear();
        }

        // MOVE TAB
        private void btnAddMove_Click(object sender, RoutedEventArgs e)
        {
            if (txtNewMove.Text.Length > 0)
            {
                if (!listMoves.Items.Contains(txtNewMove.Text))
                {
                    listMoves.Items.Add(txtNewMove.Text);
                    loadedAttackMoves.Add(CreateAttackMove(txtNewMove.Text));
                    listMoves.UpdateLayout();
                    txtNewMove.Clear();
                }
            }
        }

        private void btnRemoveMove_Click(object sender, RoutedEventArgs e)
        {
            if (listMoves.SelectedIndex >= 0)
            {
                MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to remove " + listMoves.Items.GetItemAt(listMoves.SelectedIndex).ToString() + "?",
              "Remove Move?",
                MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    loadedAttackMoves.RemoveAt(listMoves.SelectedIndex);
                    // Removing an item from a list immediately calls selection change
                    // setting the index to -1 prevents data from being overridden
                    previousAttackMoveSelectedIndex = -1;
                    listMoves.Items.RemoveAt(listMoves.SelectedIndex);
                    ResetMoveView();
                }
            }
        }

        private void ResetMoveView()
        {
            txtMovePriority.Clear();
            effectPanel.Children.Clear();

            imageMoveIcon.Source = null;
        }

        private void btnAddMoveIcon_Click(object sender, RoutedEventArgs e)
        {
            imageMoveIcon.Source = getImageFromFileBrowse();
        }

        private bool ExportMoves()
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "json|*.json";
            Nullable<bool> result = fd.ShowDialog();
            if (result == true)
            {
                File.WriteAllText(fd.FileName, SerializeMovesToJSON());
            }
            return result == null ? false : true;
        }
        private string ImportMoves()
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "json|*.json";
            Nullable<bool> result = fd.ShowDialog();
            if (result == true)
            {
                return File.ReadAllText(fd.FileName);
            }
            return "";
        }

        private void DeserializeMovesFromJSON(string json)
        {
            if (json.Equals(""))
            {
                return;
            }
            JObject myJsonNetObject = JObject.Parse(json);
            foreach (JObject attackMove in myJsonNetObject["Moves"])
            {
                loadedAttackMoves.Add(CreateAttackMove(attackMove));
            }
        }

        private AttackMove CreateAttackMove(JObject attackMoveObject)
        {
            AttackMove attackMove = new AttackMove();
            attackMove.name = attackMoveObject["Name"].ToString();
            attackMove.icon = attackMoveObject["Icon"].ToString();

            attackMove.movePriority = attackMoveObject["MovePriority"] != null ? int.Parse(attackMoveObject["MovePriority"].ToString()) : 0;
            attackMove.effects = new List<Effect>();
            Effect newEffect;
            if(attackMoveObject["Effects"] != null)
            {
                foreach (JProperty effect in (JToken)attackMoveObject["Effects"])
                {
                    newEffect = new Effect();
                    foreach (JObject attribute in effect)
                    {
                        newEffect.target = attribute["Target"] != null ? attribute["Target"].ToString() : "Opponent";
                        newEffect.element = attribute["Element"] != null ? attribute["Element"].ToString() : "Regular";
                        newEffect.pow = attribute["Pow"] != null ? int.Parse(attribute["Pow"].ToString()) : 0;
                        newEffect.repeat = attribute["Repeat"] != null ? int.Parse(attribute["Repeat"].ToString()) : 0;
                        newEffect.delay = attribute["Delay"] != null ? int.Parse(attribute["Delay"].ToString()) : 0;
                        newEffect.status = attribute["Status"] != null ? bool.Parse(attribute["Status"].ToString()) : false;
                        attackMove.effects.Add(newEffect);
                    }
                }
            }
            return attackMove;
        }

        private AttackMove CreateAttackMove(string name)
        {
            AttackMove attackMove = new AttackMove();
            attackMove.name = name;
            attackMove.icon = "";

            attackMove.movePriority = 0;
            attackMove.icon = "";

            return attackMove;
        }

        private string SerializeMovesToJSON()
        {
            JObject attackMoveObject = new JObject();
            JObject serialzedJson;
            JArray attackMoveArray = new JArray();
            JObject effectObject;
            JObject effectSerialized;
            JProperty attackMoveProperties;
            foreach (AttackMove attackMove in loadedAttackMoves)
            {
                serialzedJson = new JObject();
                effectObject = new JObject();
                serialzedJson.Add("Name", attackMove.name);
                serialzedJson.Add("Icon", attackMove.icon);
                if (attackMove.movePriority != 0)
                {
                    serialzedJson.Add("MovePriority", attackMove.movePriority);
                }
                int i = 0;
                foreach (Effect newEffect in attackMove.effects)
                {
                    effectSerialized = new JObject();
                    if (newEffect.pow != 0)
                    {
                        effectSerialized.Add("Pow", newEffect.pow);
                    }
                    if (!newEffect.element.Equals("Regular"))
                    {
                        effectSerialized.Add("Element", newEffect.element);
                    }
                    if (!newEffect.target.Equals("Opponent"))
                    {
                        effectSerialized.Add("Target", newEffect.target);
                    }
                    if (newEffect.repeat != 0)
                    {
                        effectSerialized.Add("Repeat", newEffect.repeat);
                    }
                    if (newEffect.delay != 0)
                    {
                        effectSerialized.Add("Delay", newEffect.delay);
                    }
                    if (newEffect.status != false)
                    {
                        effectSerialized.Add("Status", newEffect.status);
                    }
                    effectObject.Add(new JProperty("Effect" + ++i, effectSerialized));
                }
                serialzedJson.Add(new JProperty("Effects", effectObject));
                attackMoveArray.Add(serialzedJson);
            }
            attackMoveProperties = new JProperty("Moves", attackMoveArray);
            attackMoveObject.Add(attackMoveProperties);
            return attackMoveObject.ToString();
        }

        private void btnImportMoves_Click(object sender, RoutedEventArgs e)
        {
            if (listMoves.Items.Count > 0)
            {
                MessageBoxResult dialogResult = MessageBox.Show("Do you want to save changes to your current work?",
              "Save Changes?",
              MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    ExportMoves();
                }
            }
            listMoves.Items.Clear();
            listMoves.UpdateLayout();
            ResetMoveView();
            setInitialMoveData();
        }

        private void setInitialMoveData()
        {
            loadedAttackMoves = new List<AttackMove>();
            DeserializeMovesFromJSON(ImportMoves());
            InitializeAttackMoveData();
        }

        private void InitializeAttackMoveData()
        {
            foreach (AttackMove attackMove in loadedAttackMoves)
            {
                AddAttackMove(attackMove.name);
            }
        }

        private void AddAttackMove(string name)
        {
            if (!listMoves.Items.Contains(name))
            {
                listMoves.Items.Add(name);
                listMoves.UpdateLayout();
            }
        }

        private void btnExportMoves_Click(object sender, RoutedEventArgs e)
        {
            SaveAttackMove(previousAttackMoveSelectedIndex);
            ExportMoves();
        }

        private void listMoves_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SaveAttackMove(previousAttackMoveSelectedIndex);
            loadAttackMoveOntoScreen(listMoves.SelectedIndex);
            previousAttackMoveSelectedIndex = listMoves.SelectedIndex;
        }

        private void SaveAttackMove(int attackMoveIndex)
        {
            if (attackMoveIndex < 0)
            {
                return;
            }
            AttackMove attackMove = CreateAttackMove(loadedAttackMoves[attackMoveIndex].name);

            attackMove.movePriority = ConvertTextBoxToInt(txtMovePriority);
            List<Effect> effects = new List<Effect>();
            Effect newEffect;
            foreach (EffectBox box in MoveEffects)
            {
                newEffect = new Effect();
                newEffect.target = box.targets.SelectedItem.ToString();
                newEffect.element = box.elements.SelectedItem.ToString();
                newEffect.pow = ConvertTextBoxToInt(box.pow);
                newEffect.repeat = ConvertTextBoxToInt(box.repeat);
                newEffect.delay = ConvertTextBoxToInt(box.delay);
                newEffect.status = (bool)box.status.IsChecked;
                effects.Add(newEffect);
            }
            attackMove.effects = effects;
            if (imageMoveIcon.Source != null)
            {
                string imageMoveString = imageMoveIcon.Source.ToString();
                attackMove.icon = imageMoveString.Substring(imageMoveString.LastIndexOf('/') + 1);
            }

            loadedAttackMoves.RemoveAt(attackMoveIndex);
            loadedAttackMoves.Insert(attackMoveIndex, attackMove);
        }

        private void loadAttackMoveOntoScreen(int attackMoveIndex)
        {
            if (attackMoveIndex == NO_SELECTION)
            {
                return;
            }

            AttackMove attackMove = loadedAttackMoves[attackMoveIndex];
            txtMovePriority.Text = attackMove.movePriority.ToString();

            MoveEffects.Clear();
            effectPanel.Children.Clear();
            if(attackMove.effects != null)
            {
                foreach (Effect newEffect in attackMove.effects)
                {
                    AddEffect(newEffect);
                }
            }

            string attackMoveIconSource = filePrefix + attackMove.icon.ToString();
            if (File.Exists(attackMoveIconSource))
            {
                imageMoveIcon.Source = new BitmapImage(new Uri(attackMoveIconSource));
            }
            else
            {
                imageMoveIcon.Source = null;
            }
        }


        private void btnAddEffect_Click(object sender, RoutedEventArgs e)
        {
            AddEffect(null);
        }

        private void btnRemoveEffect_Click(object sender, RoutedEventArgs e)
        {
            foreach (EffectBox box in MoveEffects)
            {
                if (box.remove == e.Source)
                {
                    MoveEffects.Remove(box);
                    effectPanel.Children.Remove(box.remove);
                    effectPanel.Children.Remove(box.targets);
                    effectPanel.Children.Remove(box.elements);
                    effectPanel.Children.Remove(box.pow);
                    effectPanel.Children.Remove(box.repeat);
                    effectPanel.Children.Remove(box.delay);
                    effectPanel.Children.Remove(box.status);
                    break;
                }
            }
        }

        private void AddEffect(Effect effect)
        {
            // four total effects in a move
            if (MoveEffects.Count > 3)
            {
                return;
            }
            EffectBox box = new EffectBox();
            box.targets = CreateComboBox(targets);
            box.targets.Width = 90;
            box.targets.HorizontalAlignment = HorizontalAlignment.Left;
            box.elements = CreateComboBox(elements);
            box.elements.Width = 90;
            box.elements.HorizontalAlignment = HorizontalAlignment.Left;
            box.pow.Width = 35;
            box.pow.HorizontalAlignment = HorizontalAlignment.Left;
            box.repeat.Width = 35;
            box.repeat.HorizontalAlignment = HorizontalAlignment.Left;
            box.delay.Width = 35;
            box.delay.HorizontalAlignment = HorizontalAlignment.Left;
            box.status.Width = 35;
            box.status.HorizontalAlignment = HorizontalAlignment.Left;
            box.remove.Width = 50;
            box.remove.Content = "Remove";
            box.remove.HorizontalAlignment = HorizontalAlignment.Left;
            box.remove.Click += btnRemoveEffect_Click;
            if (effect == null)
            {
                box.pow.Text = "0";
                box.repeat.Text = "0";
                box.delay.Text = "0";
                box.status.IsChecked = false;
            }
            else
            {
                SetComboBoxIndex(box.elements, effect.element);
                SetComboBoxIndex(box.targets, effect.target);
                box.pow.Text = effect.pow.ToString();
                box.repeat.Text = effect.repeat.ToString();
                box.delay.Text = effect.delay.ToString();
                box.status.IsChecked = effect.status;
            }

            MoveEffects.Add(box);
            effectPanel.Children.Add(box.targets);
            effectPanel.Children.Add(box.elements);
            effectPanel.Children.Add(box.pow);
            effectPanel.Children.Add(box.repeat);
            effectPanel.Children.Add(box.delay);
            effectPanel.Children.Add(box.status);
            effectPanel.Children.Add(box.remove);
        }

        private int ConvertTextBoxToInt(TextBox box)
        {
            if (!int.TryParse(box.Text, out int value))
            {
                return 0;
            }
            else
            {
                return value;
            }

        }

        // ZONES
        private void btnRemoveZone_Click(object sender, RoutedEventArgs e)
        {
            if (listZones.SelectedIndex >= 0)
            {
                MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to remove " + listZones.Items.GetItemAt(listZones.SelectedIndex).ToString() + "?",
              "Remove Zone?",
              MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    listZones.Items.RemoveAt(listZones.SelectedIndex);
                }
            }
        }

        private void btnAddZoneImageSmall_Click(object sender, RoutedEventArgs e)
        {
            imageZoneSmall.Source = getImageFromFileBrowse();
        }

        private void btnAddZoneImageLarge_Click(object sender, RoutedEventArgs e)
        {
            imageZoneLarge.Source = getImageFromFileBrowse();
        }

        private void btnAddZone_Click(object sender, RoutedEventArgs e)
        {
            if (txtAddZone.Text.Length > 0)
            {
                if (!listZones.Items.Contains(txtAddZone.Text))
                {
                    listZones.Items.Add(txtAddZone.Text);
                    listZones.UpdateLayout();
                    txtAddZone.Clear();
                }
            }
        }

        private void btnExportZones_Click(object sender, RoutedEventArgs e)
        {
            ExportChanges();
        }

        private void btnImportZones_Click(object sender, RoutedEventArgs e)
        {
            if (listZones.Items.Count > 0)
            {
                MessageBoxResult dialogResult = MessageBox.Show("Do you want to save changes to your current work?",
              "Save Changes?",
              MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    ExportChanges();
                }
            }
            setInitialData();
        }

        private int GetMonsterIndex(string name)
        {
            foreach (Monster monster in loadedMonsters)
            {
                if (monster.name.Equals(name))
                {
                    return loadedMonsters.IndexOf(monster);
                }
            }
            return -1;
        }
    }
}
